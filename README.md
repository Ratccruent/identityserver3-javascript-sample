# IdentityServer3 Javascript implementation #

This sample is an IdServ3 implementation that uses a Javascript client to make requests

### What is this repository for? ###

* Standalone IdentityServer3 instance that uses Javascript and an oidc manager to make requests to the server

### How do I get set up? ###

* Clone the repo
* Run the build - this may include some package updates
* Ensure the node packages are properly installed by checking \JavaScriptExample\JsApplication\node_modules
* Hit F5 to run the build, making sure that the JsApplication project is set as the Startup Project

* If the application builds and runs, but the pop up doesn't seem to work, try either running the IdentityServer project separately to make sure a valid SSL certificate gets generated for the application, and double-check that your System Variables include a NODE_PATH variable set to your node install directory